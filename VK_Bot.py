from vkbottle.bot import Bot, Message
from vkbottle.tools import PhotoMessageUploader
from vkbottle.dispatch.rules.base import AttachmentTypeRule, PayloadRule
from vkbottle import Keyboard, EMPTY_KEYBOARD, Text, KeyboardButtonColor
import requests
import os
import glob

# locals
import CTDeterminator
import MakeUpLipsMaker
import MakeUpShadowsMaker
import Status


import logging

logging.basicConfig(level=logging.INFO)


from dotenv import load_dotenv
import os
load_dotenv('.env')
VK_TOKEN = os.environ['VK_TOKEN']

bot = Bot(token=VK_TOKEN)

CTDeterminator = CTDeterminator.CTDeterminator()
MakeUpLipsMaker = MakeUpLipsMaker.MakeUpLipsMaker()
MakeUpShadowsMaker = MakeUpShadowsMaker.MakeUpShadowsMaker()
Status = Status.Status()


# keyboards

button_keep = Text("Оставь текущий", {"cmd": "keep"})
button_clear = Text("Без помады", {"cmd": "clear"})
button_start = Text("Начать!", {"cmd": "start"})
button_make = Text("Примерить макияж!", {"cmd": "make"})
button_what = Text("Help", {"cmd": "what"})
button_end = Text("Хочу закончить", {"cmd": "end"})
button_lips = Text("Примерить помаду!", {"cmd": "lips"})
button_shadows = Text("Примерить тени!", {"cmd": "shadows"})

button_back4 = Text("Оставить как есть", {"cmd": "keep"})
button_default = Text("Сменить начальный цвет", {"cmd": "default"})
button_shade = Text("Корректировать оттенок", {"cmd": "cshade"})
button_result = Text("Покажи результат!", {"cmd": "result"})
button_clear_shadows = Text("Без теней", {"cmd": "clear"})

button_confirm = Text("Подтверждаю", {"cmd": "confirm"})
button_retract = Text("Вернуться к предыдущей сессии", {"cmd": "retract"})


button1 = Text("1", {"cmd": "shade"})
button2 = Text("2", {"cmd": "shade"})
button3 = Text("3", {"cmd": "shade"})
button4 = Text("4", {"cmd": "shade"})
button5 = Text("5", {"cmd": "shade"})
button6 = Text("6", {"cmd": "shade"})
button7 = Text("7", {"cmd": "shade"})
button8 = Text("8", {"cmd": "shade"})
button9 = Text("9", {"cmd": "shade"})

button_1 = Text("1", {"cmd": "colour"})
button_2 = Text("2", {"cmd": "colour"})
button_3 = Text("3", {"cmd": "colour"})
button_4 = Text("4", {"cmd": "colour"})


keyboard1 = Keyboard(one_time=False).add(button_start).get_json()
keyboard2 = Keyboard(one_time=False).add(button_make).row() \
    .add(button_what).row().add(button_end).get_json()
keyboard3 = Keyboard(one_time=False).add(button_lips).row().add(button_shadows).row() \
    .add(button_what).row().add(button_end).get_json()
keyboard5 = Keyboard(one_time=False).add(button_result).row().add(button_lips).row() \
    .add(button_shadows).row().add(button_what).add(button_end).get_json()
keyboard6 = Keyboard(one_time=False).add(button1).add(button2).row().add(button3).add(button4)\
    .row().add(button_clear).add(button_back4). \
    row().add(button_what).add(button_end).get_json()
keyboard7 = Keyboard(one_time=False).add(button1).add(button2).add(button3).row()\
    .add(button4).add(button5).add(button6).row()\
    .add(button7).add(button8).add(button9).row()\
    .add(button_clear_shadows).add(button_back4).add(button_what).add(button_end).get_json()

keyboard8 = Keyboard(one_time=False).add(button_shade).row().add(button_default) \
    .row().add(button_shadows).row().add(button_what).add(button_end).get_json()
keyboard9 = Keyboard(one_time=False).add(button_shade).row().add(button_default).row() \
    .add(button_lips).row().add(button_what).add(button_end).get_json()

keyboard10 = Keyboard(one_time=False).add(button_1).row().add(button_3).add(button_keep).add(button_4)\
    .row().add(button_2).row().add(button_what).add(button_end).get_json()
keyboard10lips = Keyboard(one_time=False).add(button_1).add(button_keep).add(button_2)\
    .row().add(button_what).add(button_end).get_json()

keyboard4 = Keyboard(one_time=False).add(button_confirm, color=KeyboardButtonColor.POSITIVE).row() \
    .add(button_retract, color=KeyboardButtonColor.NEGATIVE).get_json()

keyboards = [keyboard1, keyboard2, keyboard3, keyboard4, keyboard5, keyboard6, keyboard7, keyboard8, keyboard9,
             keyboard10]


@bot.on.message(AttachmentTypeRule("photo"))
async def photo_handler(message: Message):
    await message.answer("Скачивание фотографии, пожалуйста ждите")
    image = requests.get(message.attachments[0].photo.sizes[-1].url).content
    if os.path.exists(f"./tmp/{message.chat_id}.jpg"):
        with open(f"./tmp/{message.chat_id}new.jpg", 'wb') as handler:
            handler.write(image)
        await message.answer("Ты действительно хочешь начать работу с новой фотографией?", keyboard=keyboard4)
    else:
        for filename in glob.glob(f"./tmp/{message.chat_id}*"):
            os.remove(filename)
        with open(f"./tmp/{message.chat_id}.jpg", 'wb') as handler:
            handler.write(image)
        user_id = message.chat_id
        Status.SetDefaultStatus(user_id)
        (rsp, colortype) = CTDeterminator.Determine(message.chat_id)

        if rsp == -1:
            await message.answer('Лицо не обнаружено :( \n Попробуй прислать другую фотографию', keyboard=EMPTY_KEYBOARD)
        elif rsp == -2:
            await message.answer(
                "Ой, вас так много! Я не смогу справиться с такой красотой сразу!! Сделайте, пожалуйста, фото с одним "
                "человеком.", keyboard=EMPTY_KEYBOARD)
        else:
            with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
                f.write("2")
            await message.answer("Давай сделаем тебе сногcшибательный макияж!\n"
                                 "Для подробной информации нажимай Help", keyboard=keyboard2)


@bot.on.message(command="start")
@bot.on.message(payload={"cmd": "start"})
async def hi_handler(message: Message):
    users_info = await bot.api.users.get(message.from_id)
    await message.answer(f"Привет, {users_info[0].first_name}! Я - твой личный карманный стилист. "
                         f"Давай вместе подберём тебе идеальный мейк на сегодня :)\n"
                         f"Пожалуйста, сделай фото своего лица. "
                         f"Желательно, при хорошем освещении и БЕЗ МАКИЯЖА. "
                         f"Отправь его КАК ФОТО, не файл. Вот пример фотки:",  keyboard=EMPTY_KEYBOARD)
    photo = await PhotoMessageUploader(bot.api).upload("./11.jpg")
    await message.answer("Чем ровнее получится кадр, тем лучше я смогу помочь тебе!",
                         attachment=photo, keyboard=EMPTY_KEYBOARD)


@bot.on.message(payload={"cmd": "confirm"})
async def confirm_handler(message: Message):
    await message.answer("Принято!")
    with open(f"./tmp/{message.chat_id}new.jpg", 'rb') as image:
        with open(f"./tmp/{message.chat_id}.jpg", 'wb') as original:
            for line in image:
                original.write(line)
    os.remove(f"./tmp/{message.chat_id}new.jpg")
    user_id = message.chat_id
    Status.SetDefaultStatus(user_id)
    (rsp, colortype) = CTDeterminator.Determine(message.chat_id)

    if rsp == -1:
        await message.answer('Лицо не обнаружено :( \n Попробуй прислать другую фотографию', keyboard=EMPTY_KEYBOARD)
    elif rsp == -2:
        await message.answer(
            "Ой, вас так много! Я не смогу справиться с такой красотой сразу!! Сделайте, пожалуйста, фото с одним "
            "человеком.", keyboard=EMPTY_KEYBOARD)
    else:
        with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
            f.write("2")
        await message.answer("Давай сделаем тебе сногcшибательный макияж!\n"
                             "Для подробной информации нажимай Help", keyboard=keyboard2)


@bot.on.message(payload={"cmd": "retract"})
async def retract_handler(message: Message):
    await message.answer("Принято! \n Сейчас пока такой результат:")
    id = 2
    with open(f"./tmp/{message.chat_id}keyboard.txt", "r") as f:
        id = int(f.read()) - 1
    MakeUpLipsMaker.DrawLips(message.chat_id)
    MakeUpShadowsMaker.DrawShadows(message.chat_id)
    photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}res.jpg")
    await message.answer(attachment=photo)
    colortype = CTDeterminator.GetColortype(message.chat_id)
    if id == 5:
        await message.answer("Давай продолжим работу!")
        photo = await PhotoMessageUploader(bot.api).upload(f"./Makeup_colours/{colortype}lips.jpg")
        await message.answer("Выбирай цвет:", attachment=photo, keyboard=keyboards[id])
    elif id == 6:
        await message.answer("Давай продолжим работу!")
        photo = await PhotoMessageUploader(bot.api).upload(f"./Makeup_colours/{colortype}shadows.jpg")
        await message.answer("Выбирай цвет:", attachment=photo, keyboard=keyboards[id])
    elif id == 9:
        await message.answer("Давай продолжим работу!")
        file = open(f"tmp/{message.chat_id}section.txt", "r")
        type = file.readline().split()[0]
        file.close()
        if type == "lips":
            photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}lips.jpg")
        else:
            photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}shadows.jpg")
        await message.answer("Выбирай оттенок:", attachment=photo, keyboard=keyboards[id])
    else:
        await message.answer("Давай продолжим работу!", keyboard=keyboards[id])


@bot.on.message(payload={"cmd": "what"})
async def help_handler(message: Message):
    await message.answer("Кнопка Примерить макияж! поможет подобрать цвет помады и теней. "
                         "Нажми на нее, потом выбери, что хочешь нанести сначала - тени или помаду.\n"
                         "Если выберешь помаду - сначала я покажу тебе 4 оттенка, которые подходят твоему цвету. "
                         "Выбери 1 из них или нажми Без помады - вернешься на шаг назад.\n "
                         "Когда выберешь цвет, я покажу, как помада будет смотреться на тебе! "
                         "Можешь оставить этот оттенок, а можешь нажать Корректировать оттенок и подправить его.\n"
                         "С тенями кнопки аналогичные. \n"
                         "Кнопка Оставить как есть вернет тебя к предыдущему действию, "
                         "а кнопка Хочу остановиться - если ты устанешь примерять образы. "
                         "(Если при выборе оттенка нажать Оставить как есть - сохранится последний выбор.) \n"
                         "А теперь давай творить красоту!")


@bot.on.message(payload={"cmd": "make"})
async def do_make_handler(message: Message):
    user_id = message.chat_id
    Status.SetDefaultStatus(user_id)
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write("3")
    user_id = message.chat_id
    colortype = CTDeterminator.GetColortype(user_id)
    await message.answer(recs[colortype])
    await message.answer("Выбирай, что хочешь примерить:", keyboard=keyboard3)


@bot.on.message(payload={"cmd": "end"})
async def end_handler(message: Message):
    for filename in glob.glob(f"./tmp/{message.chat_id}*"):
        os.remove(filename)
    await message.answer("Было классно с тобой работать! "
                                "Если хочешь получить рекомендации по другой фотке, нажми кнопку Начать! \n"
                                "И еще - если тебе не сложно, оставь отзыв по моей работе c этой фотографией. "
                                "Ссылка на форму: https://forms.gle/Dy8q2tSV8jpsMEJb9",
                         keyboard=keyboard1)


@bot.on.message(payload={"cmd": "lips"})
async def lips_handler(message: Message):
    user_id = message.chat_id
    file = open(f"tmp/{user_id}section.txt", "w")
    file.write("lips")
    file.close()
    user_id = message.chat_id
    colortype = CTDeterminator.GetColortype(user_id)
    photo = await PhotoMessageUploader(bot.api).upload(f"./Makeup_colours/{colortype}lips.jpg")
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write("6")
    await message.answer("Выбери один из этих оттенков", attachment=photo, keyboard=keyboard6)


@bot.on.message(PayloadRule([{"cmd": "shade"}, {"cmd": "clear"}]))
async def choice_handler(message: Message):
    if message.text == 'Без помады' or message.text == 'Без теней':
        choice = 0
    else:
        choice = int(message.text)
    file = open(f"tmp/{message.chat_id}section.txt", "r")
    type = file.readline().split()[0]
    file.close()
    if type == "lips":
        Status.SetStatus(message.chat_id, is_lips=True, do_make=1 if choice > 0 else 0)
        if choice > 0:
            MakeUpLipsMaker.SetDefaultLips(message.chat_id, choice)
    else:
        Status.SetStatus(message.chat_id, is_lips=False, do_make=1 if choice > 0 else 0)
        if choice > 0:
            MakeUpShadowsMaker.SetDefaultShadows(message.chat_id, choice)
    status = list(Status.Getstatus(user_id=message.chat_id))
    MakeUpLipsMaker.DrawLips(message.chat_id)
    MakeUpShadowsMaker.DrawShadows(message.chat_id)
    photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}res.jpg")
    await message.answer(attachment=photo)
    if (type == "lips" and status[0]) or (type == "shadows" and status[1]):
        key = "8" if type=="lips" else "9"
        with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
            f.write(key)
        await message.answer("Вот что получилось! Продолжим? :)",
                               keyboard=keyboard8 if type == "lips" else keyboard9)
    else:
        status = list(Status.Getstatus(message.chat_id))
        ans = "Сейчас пока такой результат\n\nПомада: " + ("нет\n" if status[0] == 0 else "выбрано\n")
        ans += "Тени: " + ("нет\n" if status[1] == 0 else "выбрано\n")
        key = "5" if status[0] or status[1] else "3"
        with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
            f.write(key)
        await message.answer(ans, keyboard=keyboard5 if status[0] or status[1] else keyboard3)


@bot.on.message(payload={"cmd": "cshade"})
async def correct_handler(message: Message):
    user_id = message.chat_id
    file = open(f"tmp/{user_id}section.txt", "r")
    type = file.readline().split()[0]
    file.close()
    if type == "lips":
        MakeUpLipsMaker.GenerateNextRange(user_id)
    else:
        MakeUpShadowsMaker.GenerateNextRange(user_id)
    if type == "lips":
        photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}lips.jpg")
    else:
        photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}shadows.jpg")
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write("10")
    await message.answer("Выбирай оттенок", attachment=photo,
                         keyboard=keyboard10)


@bot.on.message(payload={"cmd": "keep"})
async def stay_handler(message: Message):
    MakeUpLipsMaker.DrawLips(user_id=message.chat_id)
    MakeUpLipsMaker.DrawLips(message.chat_id)
    MakeUpShadowsMaker.DrawShadows(message.chat_id)
    photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}res.jpg")
    await message.answer(attachment=photo)
    status = list(Status.Getstatus(message.chat_id))
    ans = "Сейчас пока такой результат\n\nПомада: " + ("нет\n" if status[0] == 0 else "выбрано\n")
    ans += "Тени: " + ("нет\n" if status[1] == 0 else "выбрано\n")
    key = "5" if status[0] or status[1] else "3"
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write(key)
    await message.answer(ans, keyboard=keyboard5 if status[0] or status[1] else keyboard3)


@bot.on.message(payload={"cmd": "default"})
async def change_default_handler(message: Message):
    user_id = message.chat_id
    file = open(f"tmp/{user_id}section.txt", "r")
    type = file.readline().split()[0]
    file.close()
    user_id = message.chat_id
    colortype = CTDeterminator.GetColortype(user_id)
    if type == "lips":
        photo = await PhotoMessageUploader(bot.api).upload(f"./Makeup_colours/{colortype}lips.jpg")
    else:
        photo = await PhotoMessageUploader(bot.api).upload(f"./Makeup_colours/{colortype}shadows.jpg")
    key = "6" if type == "lips" else "7"
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write(key)
    await message.answer('Выбери один из этих оттенков', attachment=photo,
                           keyboard=keyboard6 if type == "lips" else keyboard7)


@bot.on.message(payload={"cmd": "colour"})
async def choice_handler(message: Message):
    choice = int(message.text)
    file = open(f"tmp/{message.chat_id}section.txt", "r")
    type = file.readline().split()[0]
    file.close()
    if type == "lips":
        Status.SetStatus(message.chat_id, is_lips=True, do_make=1 if choice > 0 else 0)
        if choice > 0:
            MakeUpLipsMaker.SetCustomLips(message.chat_id, choice)
    else:
        Status.SetStatus(message.chat_id, is_lips=False, do_make=1 if choice > 0 else 0)
        if choice > 0:
            MakeUpShadowsMaker.SetCustomShadows(message.chat_id, choice)
    status = list(Status.Getstatus(user_id=message.chat_id))
    MakeUpLipsMaker.DrawLips(message.chat_id)
    MakeUpShadowsMaker.DrawShadows(message.chat_id)
    photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}res.jpg")
    await message.answer(attachment=photo)
    user_id = message.chat_id
    file = open(f"tmp/{user_id}section.txt", "r")
    type = file.readline().split()[0]
    file.close()
    if type == "lips":
        MakeUpLipsMaker.GenerateNextRange(user_id)
    else:
        MakeUpShadowsMaker.GenerateNextRange(user_id)
    if type == "lips":
        photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}lips.jpg")
    else:
        photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}shadows.jpg")
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write("10")
    await message.answer("Если хочешь, подкорректируй оттенок еще раз",
                         attachment=photo,
                         keyboard=keyboard10)


@bot.on.message(payload={"cmd": "result"})
async def result_handler(message: Message):
    MakeUpLipsMaker.DrawLips(message.chat_id)
    MakeUpShadowsMaker.DrawShadows(message.chat_id)
    photo = await PhotoMessageUploader(bot.api).upload(f"tmp/{message.chat_id}res.jpg")
    await message.answer(attachment=photo)
    status = list(Status.Getstatus(message.chat_id))
    ans = "Сейчас пока такой результат\n\nПомада: " + ("нет\n" if status[0] == 0 else "выбрано\n")
    ans += "Тени: " + ("нет\n" if status[1] == 0 else "выбрано\n")
    key = "5" if status[0] or status[1] else "3"
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write(key)
    await message.answer(ans, keyboard=keyboard5 if status[0] or status[1] else keyboard3)


@bot.on.message(payload={"cmd": "shadows"})
async def shadows_handler(message: Message):
    user_id = message.chat_id
    file = open(f"tmp/{user_id}section.txt", "w")
    file.write("shadows")
    file.close()
    user_id = message.chat_id
    colortype = CTDeterminator.GetColortype(user_id)
    photo = await PhotoMessageUploader(bot.api).upload(f"./Makeup_colours/{colortype}shadows.jpg")
    with open(f"./tmp/{message.chat_id}keyboard.txt", "w") as f:
        f.write("7")
    await message.answer('Выбери один из этих оттенков',
                         attachment=photo,
                         keyboard=keyboard7)


recs = {
    "winter": "Твой цветотип - зима! \n У тебя от природы яркая и контрастная внешность, которая выделяют тебя из толпы и притягивает взгляд. "
              "Работать с таким типажом нужно аккуратно – главное правильно подчеркнуть контраст, оставляя при этом образ целостным.\n"
              "Мы советуем сделать акцент на глаза: хороший smoky-eyes сделает взгляд ещё более глубоким, подчеркнув глаза на фоне бледной кожи.\n"
              "Вам больше подойдёт холодные оттенки, однако красная помада идёт вам как никому другому, а бежево-коричневая палетка теней спасёт, если нужно сделать лёгкий макияж. "
              "Правда, золотистого блеска лучше избегать.\n",
    "spring": "Твой цветотип - весна! \n Вы обладаете очень нежным и женственным типом внешности. "
              "Вам не нужно часы проводить у зеркала: достаточно лишь немного коснуться тенями век, чтобы подчеркнуть выразительность глаз. "
              "Smoky – однозначно ваш вариант, если соблюдать тёплую и светлую гамму. "
              "Мягкие переходы от одного цвета к другому смотрятся естественно в дневном варианте макияжа. "
              "Не стесняйтесь использовать тени с шиммером – вам как никому другому идёт золотистый отблеск на веках.\n"
              "А вот с более заметным макияжем можно экспериментировать - вам подойдут любые яркие тона. Яркие и тёплые – беспроигрышный вариант.\n"
              "А вот более тёмные оттенки могут перетянуть внимание исключительно на себя, сделав внешность в целом более тусклой. Хотя и их можно подобрать удачно.\n",
    "summer": "Твой цветотип - лето! \n Вы обладаете спокойной и прохладной внешностью. "
              "В ней чувствуется сдержанность и благородство, поэтому вам не нужны кричащие тона, чтобы заявить о себе. Прохладные оттенки подчеркнут аристократизм внешности, при этом можно не бояться ошибиться с оттенком: подойдут как светлые так и тёмные тени, а также разные их сочетания. " \
              "Благодаря этому вам доступны разные техники макияжа: от классических smoky-eyes до «кошачьего глаза» и графичных стрелок.\n"
              "Для более праздничного образа можно использовать тени с текстурой металлик. "
              "Она подчеркнёт прохладность цвета и не нарушит гармонию типажа.\n",
    "autumn": "Твой цветотип - осень! \n Вы обладаете очень тёплой, притягательной и почти солнечной внешностью, поэтому вам подойдут оттенки, относящиеся к схожей цветовой гамме.\n"
              "Больше ограничений при выборе почти нет – вам подойдут и светлые, и тёмные тона из всего спектра тёплой палитры. " \
              "Это позволяет без проблем создать и casual-макияж и яркий вечерний образ.\n"
              "Что касается блеска, то здесь можно использовать золотистый шиммер или же использовать матовые тени – всё зависит от ситуации и вашего желания.\n"
}


if __name__ == '__main__':
    bot.run_forever()

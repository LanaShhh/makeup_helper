# -*- coding: utf-8 -*-
import numpy as np
from catboost import CatBoostClassifier
import colorsys
from typing import Tuple
import ColourDeterminator

# Class that determines colourtype of person
class CTDeterminator(object):
    # Model predicting colortypes
    CBC = CatBoostClassifier().load_model("face_type_model.cbm", format='cbm')
    # Object to determine colours
    colour_determinator = ColourDeterminator.ColourDetermine()
    #Array of types
    types = ["winter", "spring", "summer", "autumn"]

    # Function to get HSV from RGB (returns np.array)
    def RGB_to_HSV(self, colour):
        res = colorsys.rgb_to_hsv(colour[0] / 255, colour[1] / 255, colour[2] / 255)
        return np.array([res[0] * 360, res[1] * 100, res[2] * 100])

    # Function to determine colourtype
    # Input - user_id
    # Returns 0 and colourtype if the photo is correct. Saves colortype in tmp/{user_id}type.txt
    # Returns -1 if there is no person found
    # Returns -2 if there are more than 1 person
    def Determine(self, user_id):
        # Get colours
        colours = self.colour_determinator.get_colours(f"tmp/{user_id}.jpg")
        # Checking for errors in detecting
        if colours == "no faces error":
            return -1, 0
        if colours == "too many faces error":
            return -2, 0
        # Save colours in RGB and HSV
        eye_colour, face_colour, hair_colour, eyebrow_colour = colours
        eye_colourHSV = self.RGB_to_HSV(eye_colour)
        face_colourHSV = self.RGB_to_HSV(face_colour)
        hair_colourHSV = self.RGB_to_HSV(hair_colour)
        eyebrow_colourHSV = self.RGB_to_HSV(eyebrow_colour)
        # Get an array of data
        data = np.concatenate((face_colour, face_colourHSV,
                               eye_colour, eye_colourHSV,
                               hair_colour, hair_colourHSV,
                               eyebrow_colour, eyebrow_colourHSV), axis=0)
        pred_type = self.CBC.predict(data)[0]
        # Save colourtype
        file = open(f"tmp/{user_id}type.txt", "w")
        file.write(f"{self.types[pred_type]}\n")
        file.close()
        return 0, self.types[pred_type]

    # Function to get already predicted colourtype
    def GetColortype(self, user_id):
        file = open(f"tmp/{user_id}type.txt", "r")
        colortype = [x for x in (file.readline()).split()]
        return colortype[0]
    
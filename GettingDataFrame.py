# -*- coding: utf-8 -*-
import math
import cv2
import numpy as np
import dlib
import pandas as pd
from os import listdir
import ColourDeterminator
import colorsys


# Function to get colour in HSV from RGB
def RGB_to_HSV(colour):
    res = colorsys.rgb_to_hsv(colour[0] / 255, colour[1] / 255, colour[2] / 255)
    return [res[0] * 360, res[1] * 100, res[2] * 100]


# To determine colour
colour_determinator = ColourDeterminator.ColourDetermine()
# 4 types
types = ["winter", "summer", "spring", "autumn"]
# Arrays for data
face, eyes, hair, eyebrows, type = [], [], [], [], []

# Put photos from 12 folders to 4
files_counter = 0
import shutil
for type_ in types:
    for i in range(1, 4):
        for file in listdir(f"Faces_data/{type_}{i}"):
            print(f"Copying in {type_}{i}, file {file}")
            shutil.copyfile(f"Faces_data/{type_}{i}/{file}", f"Faces_data/{type_}/{i}_{file}")
            files_counter += 1
print(f"Copied {files_counter} files")

# Get colour data for every photo in folders
for i in range(len(types)):
    for file in listdir(f"Faces_data/{types[i]}"):
        print(f"Type {types[i]}, filename {file}")
        # Get colours
        colours = colour_determinator.get_colours(f"Faces_data/{types[i]}/{file}")
        # print(colours)
        # Checking for errors in detecting
        if colours == "no faces error" or colours == "too many faces error":
            continue
        # Save colours in RGB and HSV
        eye_colour, face_colour, hair_colour, eyebrow_colour = colours
        eye_colourHSV = RGB_to_HSV(eye_colour)
        face_colourHSV = RGB_to_HSV(face_colour)
        hair_colourHSV = RGB_to_HSV(hair_colour)
        eyebrow_colourHSV = RGB_to_HSV(eyebrow_colour)
        eyes.append([int(x) for x in eye_colour] + [int(x) for x in eye_colourHSV])
        face.append([int(x) for x in face_colour] + [int(x) for x in face_colourHSV])
        hair.append([int(x) for x in hair_colour] + [int(x) for x in hair_colourHSV])
        eyebrows.append([int(x) for x in eyebrow_colour] + [int(x) for x in eyebrow_colourHSV])
        # Save type
        type.append(i)

# Get numpy arrays from data
face = np.array(face)
eyes = np.array(eyes)
hair = np.array(hair)
eyebrows = np.array(eyebrows)
type = np.array(type)[:, np.newaxis]

# Create a pandas DataFrame and save it sa csv file
faces_df = pd.DataFrame(np.concatenate((face, eyes, hair, eyebrows, type), axis=1),
                        columns=["faceR", "faceG", "faceB", "faceH", "faceS", "faceV",
                                 "eyesR", "eyesG", "eyesB", "eyesH", "eyesS", "eyesV",
                                 "hairR", "hairG", "hairB", "hairH", "hairS", "hairV",
                                 "eyebrowR", "eyebrowG", "eyebrowB", "eyebrowH", "eyebrowS", "eyebrowV",
                                 "type"])
# Save in current dir and in jupyter-context to have a research
faces_df.to_csv('faces_df.csv')
faces_df.to_csv('jupyter-context/faces_df.csv')
print(faces_df)



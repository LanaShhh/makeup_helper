import numpy as np
import cv2
import mediapipe as mp
import itertools

class ColourDetermine:
    detector = mp.solutions.face_mesh

    def get_points(self, img_path):
        """
        :param img_path:
        :return number of faces on photo
        :return if there is exactly one face, its landmarks, empty array otherwise:
        """

        with self.detector.FaceMesh(static_image_mode=True, max_num_faces=10) as face:
            image = cv2.imread(img_path)
            results = face.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            face_cnt = 0
            points = np.array([])
            if results.multi_face_landmarks != None:
                for landmarks in results.multi_face_landmarks:
                    height, width = image.shape[:2]
                    coordinates = [(point.x, point.y, point.z) for point in landmarks.landmark]
                    cur_points = np.array(np.multiply(coordinates, [width, height, width]).astype(int))
                    if len(points) == 0:
                        points = cur_points
                    else:
                        points = np.concatenate((points, cur_points))

                face_cnt = len(points) // 468
            return face_cnt, points

    def circle_average_colour(self, image, l, r, t, b):
        """
        :param image:
        :param l:
        :param r:
        :param t:
        :param b:
        :return average colour in incircle:
        """
        rad = (t[1] - b[1]) // 2
        cx, cy = (l[0] + r[0]) // 2, (t[1] + b[1]) // 2
        B, G, R = 0, 0, 0
        cells_number = 0
        image[cy, cx] = (0, 0, 0)
        for x in range(l[0], r[0]):
            for y in range(b[1], t[1]):
                if rad ** 2 / 4 <= (x - cx) ** 2 + (y - cy) ** 2 <= rad ** 2:
                    B += image[y][x][0]
                    G += image[y][x][1]
                    R += image[y][x][2]
                    cells_number += 1
        return np.array((R, G, B)) // cells_number

    def rectangle_average_colour(self, image, l, r, t, b):
        """
        :param image:
        :param l:
        :param r:
        :param t:
        :param b:
        :return average colour:
        """
        B, G, R = 0, 0, 0
        cells_number = 0
        for x in range(l[0], r[0]):
            for y in range(b[1], t[1]):
                B += image[y][x][0]
                G += image[y][x][1]
                R += image[y][x][2]
                cells_number += 1
        return np.array((R, G, B)) // cells_number

    def get_colours(self, img_path):
        """
        :param img_path:
        :return eye_colour in rgb
        :return face_colour in rgb
        :return hair_colour in rgb
        :return eyebrow_colour in tgb
        """
        number, points = self.get_points(img_path)
        image = cv2.imread(img_path)
        if number == 0:
            return "no faces error"
        if number > 1:
            return "too many faces error"
        eye_colour = self.get_eye_colour(image, points)
        face_colour = self.get_face_colour(image, points)
        hair_colour = self.get_hair_colour(image, points)
        eyebrow_colour = self.get_eyebrow_colour(image, points)
        return eye_colour, face_colour, hair_colour, eyebrow_colour


    def get_eye_colour(self, img, points):
        """
        :param img:
        :param points:
        :return eye_colour:
        """
        left_eye_indexes = list(set(itertools.chain(*self.detector.FACEMESH_LEFT_EYE)))
        right_eye_indexes = list(set(itertools.chain(*self.detector.FACEMESH_RIGHT_EYE)))
        left_eye_points = points[left_eye_indexes]
        right_eye_points = points[right_eye_indexes]
        l = sorted(left_eye_points, key=lambda point: point[0])[0]
        r = sorted(left_eye_points, key=lambda point: point[0])[-1]
        t = sorted(left_eye_points, key=lambda point: point[1])[-1]
        b = sorted(left_eye_points, key=lambda point: point[1])[0]
        left_res = self.circle_average_colour(img, l, r, t, b)
        l = sorted(right_eye_points, key=lambda point: point[0])[0]
        r = sorted(right_eye_points, key=lambda point: point[0])[-1]
        t = sorted(right_eye_points, key=lambda point: point[1])[-1]
        b = sorted(right_eye_points, key=lambda point: point[1])[0]
        right_res = self.circle_average_colour(img, l, r, t, b)
        return (left_res + right_res) // 2

    def get_face_colour(self, img, points):
        """
        :param img:
        :param points:
        :return face_colour in rgb:
        """
        face_indexes = list(set(itertools.chain(*self.detector.FACEMESH_FACE_OVAL)))
        face_points = points[face_indexes]
        left_eyebrow_ind = list(set(itertools.chain(*self.detector.FACEMESH_LEFT_EYEBROW)))
        left_eyebrow_points = points[left_eyebrow_ind]
        right_eyebrow_ind = list(set(itertools.chain(*self.detector.FACEMESH_RIGHT_EYEBROW)))
        right_eyebrow_points = points[right_eyebrow_ind]
        b = sorted(face_points, key=lambda point: point[1])[0]
        tl = sorted(left_eyebrow_points, key=lambda point: point[1])[0]
        tr = sorted(right_eyebrow_points, key=lambda point: point[1])[0]
        t = tl if tl[1] < tr[1] else tr
        l = sorted(right_eyebrow_points, key=lambda point: point[0])[0]
        r = sorted(left_eyebrow_points, key=lambda point: point[0])[-1]
        return self.rectangle_average_colour(img, l, r, t, b)

    def get_hair_colour(self, img, points):
        """
        :param img:
        :param points:
        :return hair colour in rgb:
        """
        m = sorted(points, key=lambda point: point[0])[len(points) // 2]
        l = sorted(points, key=lambda point: point[0])[0]
        r = sorted(points, key=lambda point: point[0])[-1]
        t = sorted(points, key=lambda point: point[1])[0]
        t = np.array((m[0], max(t[1] - (m[1] - l[1]) // 3, 10)))
        b = t.copy()
        b[1] = max(0, b[1] - (m[1] - l[1]) // 5)
        l[0] += 10
        r[0] -= 10
        return self.rectangle_average_colour(img, l, r, t, b)

    def get_eyebrow_colour(self, img, points):
        left_eyebrow_ind = list(set(itertools.chain(*self.detector.FACEMESH_LEFT_EYEBROW)))
        left_eyebrow_points = points[left_eyebrow_ind]
        right_eyebrow_ind = list(set(itertools.chain(*self.detector.FACEMESH_RIGHT_EYEBROW)))
        right_eyebrow_points = points[right_eyebrow_ind]
        l = sorted(left_eyebrow_points, key=lambda point: point[0])[0]
        r = sorted(left_eyebrow_points, key=lambda point: point[0])[-1]
        b = sorted(left_eyebrow_points, key=lambda point: point[1])[0]
        t = sorted(left_eyebrow_points, key=lambda point: point[1])[-1]
        b[1] += (t[1] - b[1]) // 6
        t[1] -= (t[1] - b[1]) // 6
        l[0] += (r[0] - l[0]) // 6
        r[0] -= (r[0] - l[0]) // 6
        left_res = self.rectangle_average_colour(img, l, r, t, b)
        l = sorted(right_eyebrow_points, key=lambda point: point[0])[0]
        r = sorted(right_eyebrow_points, key=lambda point: point[0])[-1]
        b = sorted(right_eyebrow_points, key=lambda point: point[1])[0]
        t = sorted(right_eyebrow_points, key=lambda point: point[1])[-1]
        b[1] += (t[1] - b[1]) // 6
        t[1] -= (t[1] - b[1]) // 6
        l[0] += (r[0] - l[0]) // 6
        r[0] -= (r[0] - l[0]) // 6
        right_res = self.rectangle_average_colour(img, l, r, t, b)
        return (left_res + right_res) // 2


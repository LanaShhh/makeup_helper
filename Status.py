# Class that sets status of lips and shadows separately (no makeup - 0 / have makeup - 1)
class Status(object):
    # Set default status - 0 0
    def SetDefaultStatus(self, user_id):
        file = open(f"tmp/{user_id}status.txt", "w")
        file.write("0 0")
        file.close()

    # Set makeup changes
    # is_lips=1 - change lips
    # is_shadows=1 - change shadows
    # do_make = 0 no makeup / 1 have makeup
    def SetStatus(self,
                  user_id,
                  is_lips: bool = False,
                  is_shadows: bool = False,
                  do_make: int = 0):
        file = open(f"tmp/{user_id}status.txt", "r")
        status = [int(x) for x in file.readline().split()]
        file.close()
        if (is_lips):
            status[0] = do_make
        else:
            status[1] = do_make
        file = open(f"tmp/{user_id}status.txt", "w")
        file.write(f"{status[0]} {status[1]}")
        file.close()
        return

    # Get current status
    def Getstatus(self,
                  user_id):
        file = open(f"tmp/{user_id}status.txt")
        status = (int(x) for x in (file.readline()).split())
        return status
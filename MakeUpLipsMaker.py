# -*- coding: utf-8 -*-
import cv2
import numpy as np
import dlib
import colorsys
import Status


Status = Status.Status()


class MakeUpLipsMaker(object):
    # Face detector
    detector = dlib.get_frontal_face_detector()
    # Detector of 68 face landmarks
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    # RGB
    DefaultLips = {
        "autumn": [(191, 128, 87), (206, 112, 75), (200, 110, 102), (164, 75, 71)],
        "spring": [(230, 84, 133), (245, 91, 86), (215, 58, 101), (168, 6, 89)],
        "summer": [(204, 127, 163), (255, 174, 201), (233, 138, 162), (240, 136, 181)],
        "winter": [(131, 42, 91), (125, 40, 55), (226, 0, 63), (174, 31, 47)],
    }

    # Save person choice of default colour
    def SetDefaultLips(self,
                       user_id,
                       choice: int):
        file = open(f"tmp/{user_id}type.txt", "r")
        colortype = [x for x in (file.readline()).split()][0]
        file.close()
        file = open(f"tmp/{user_id}lips.txt", "w")
        # Save colour in RGB
        color = self.DefaultLips[f"{colortype}"][choice - 1]
        file.write(f"{color[0]} {color[1]} {color[2]}")
        file.close()
        return

    # Step of changing colours
    STEP = 1 / 8

    # Save custom user choice 1, 2, 3 или 4
    #    1
    # 3     4
    #    2
    # Call  this function only after SetDefaultLips
    def SetCustomLips(self,
                      user_id,
                      choice: int):
        # Get current colour
        file = open(f"tmp/{user_id}lips.txt", "r")
        # Get previosly picked colour
        prev_color = [float(x) for x in (file.readline()).split()]
        # Go to the next colour in RGB
        color = (prev_color[0] / 255, prev_color[1] / 255, prev_color[2] / 255)
        color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        if choice == 1:
            color[2] = min(1, color[2] + self.STEP)
        elif choice == 2:
            color[2] = max(0, color[2] - self.STEP)
        elif choice == 3:
            color[1] = max(0, color[1] - self.STEP)
        elif choice == 4:
            color[1] = min(1, color[1] + self.STEP)
        color = colorsys.hsv_to_rgb(color[0], color[1], color[2])
        # Save new colour as current
        file = open(f"tmp/{user_id}lips.txt", "w")
        file.write(f"{color[0] * 255} {color[1] * 255} {color[2] * 255}")
        file.close()
        return

    # Function that draw lips
    def DrawLips(self,
                 user_id):
        # Read lips colour
        try:
            file = open(f"tmp/{user_id}lips.txt", "r")
            color = [float(x) for x in file.readline().split()]
            file.close()
        except:
            color = [-1, -1, -1]
        # Get the image
        img = cv2.imread(f"tmp/{user_id}.jpg")
        # Check the status of lips
        status = list(Status.Getstatus(user_id=user_id))
        if status[0] == 0:
            cv2.imwrite(f"tmp/{user_id}res.jpg", img)
            return
        # Black-white for predictor and detector
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        faces = self.detector(gray_img)
        face = faces[0]
        landmarks = self.predictor(gray_img, face)
        points = []
        for n in range(68):
            x = landmarks.part(n).x
            y = landmarks.part(n).y
            points.append([x, y])
        cur_points1 = np.array(points[48:55] + points[64:59:-1])
        cur_points2 = np.array(points[59:53:-1] + points[64:68] + [points[60]] + [points[48]])
        mask1 = np.zeros_like(img)
        mask1 = cv2.fillPoly(mask1, [cur_points1], (255, 255, 255))
        mask2 = np.zeros_like(img)
        mask2 = cv2.fillPoly(mask2, [cur_points2], (255, 255, 255))
        mask = cv2.bitwise_or(mask1, mask2)
        real_lips = cv2.bitwise_and(img, mask)
        lips_mask = mask
        # colored_lips - lips with colour named color on a black background
        colored_lips = np.zeros_like(lips_mask)
        colored_lips = cv2.bitwise_and(lips_mask, colored_lips)
        colored_lips[:] = color
        colored_lips = cv2.bitwise_and(colored_lips, lips_mask)
        # result is a result picture
        result = cv2.addWeighted(img, 1, cv2.GaussianBlur(real_lips, (3, 3), 1000), -1, 0)
        # Change real lips into HSV
        real_lips = cv2.cvtColor(real_lips, cv2.COLOR_RGB2HSV)
        colored_lips = cv2.cvtColor(colored_lips, cv2.COLOR_RGB2HSV)
        # Now real lips are colored
        real_lips[:, :, 0:2] = colored_lips[:, :, 0:2]
        real_lips[:, :, 2] = np.maximum(0, colored_lips[:, :, 2] - 0.25 * (100 - real_lips[:, :, 2]))
        # real_lips[:, :, 2] = np.maximum(0, np.minimum(100, colored_lips[:, :, 2] + real_lips[:, :, 2]))
        # Change new real lips into RGB
        real_lips = cv2.cvtColor(real_lips, cv2.COLOR_HSV2RGB)
        real_lips = cv2.GaussianBlur(real_lips, (3, 3), 1000)  ##was (5, 5), 10
        # Need to change RGB to BGR
        real_lips = real_lips[:, :, ::-1]
        # Add new lips to a picture
        result = cv2.addWeighted(result, 1, real_lips, 1, 0)
        # Save picture
        cv2.imwrite(f"tmp/{user_id}res.jpg", result)
        return

    # Для отрисовки оттенков на подложке
    dots = [
        ((205, 5), (395, 195)),
        ((205, 405), (395, 595)),
        ((5, 205), (195, 395)),
        ((405, 205), (595, 395)),
        ((205, 205), (395, 395))
    ]

    # Generate a oic with 2 new colours and the current one
    def GenerateNextRange(self,
                          user_id):
        # White background
        img = np.zeros((600, 600, 3), np.uint8)
        img[:] = (255, 255, 255)
        # Read chosen colour
        file = open(f"tmp/{user_id}lips.txt")
        color = [float(x) for x in (file.readline()).split()]
        file.close()
        colors = []
        # Top in BGR
        color = (color[0] / 255, color[1] / 255, color[2] / 255)
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[2] = min(1, cur_color[2] + self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Bottom in BGR
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[2] = max(0, cur_color[2] - self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Left in BGR
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[1] = max(0, cur_color[1] - self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Right in BGR
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[1] = min(1, cur_color[1] + self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Current colour in BGR
        colors.append((color[2], color[1], color[0]))
        # Draw colours
        for i in range(5):
            cv2.rectangle(
                img,
                self.dots[i][0], self.dots[i][1],
                (colors[i][0] * 255, colors[i][1] * 255, colors[i][2] * 255),
                cv2.FILLED)
        cv2.imwrite(f'tmp/{user_id}lips.jpg', img)
        return



M = MakeUpLipsMaker()
M.DrawLips("-1718467638")

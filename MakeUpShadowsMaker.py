# -*- coding: utf-8 -*-
import cv2
import numpy as np
import dlib
import colorsys
import Status


Status = Status.Status()


def mid(p1, p2, a):
    return list([int((p1[0] + a * p2[0]) / (1 + a)), int((p1[1] + a * p2[1]) / (1 + a))])



class MakeUpShadowsMaker(object):
    # Face detector
    detector = dlib.get_frontal_face_detector()
    # Detector of 68 face landmarks
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    # RGB
    DefaultShadows = {
        "autumn": [
            (223, 187, 161), (147, 110, 92), (100, 72, 59),
            (158, 86, 88), (179, 113, 91), (121, 111, 76),
            (209, 142, 133), (179, 140, 133), (187, 172, 142)
        ],
        "spring": [
            (237, 215, 191), (224, 190, 161), (79, 56, 48),
            (180, 124, 163), (29, 123, 159), (43, 97, 64),
            (98, 63, 119), (197, 75, 51), (213, 172, 66)
        ],
        "summer": [
            (237, 216, 189), (211, 180, 170), (116, 93, 88),
            (85, 158, 173), (204, 121, 143), (189, 76, 102),
            (34, 128, 166), (222, 188, 202), (177, 157, 176)
        ],
        "winter": [
            (237, 216, 189), (89, 74, 69), (35, 35, 35),
            (40, 93, 63), (50, 87, 105), (32, 60, 74),
            (171, 149, 170), (93, 59, 84), (91, 37, 55)
        ]
    }

    # Save person choice of default colour
    def SetDefaultShadows(self,
                       user_id,
                       choice: int):
        file = open(f"tmp/{user_id}type.txt", "r")
        colortype = [x for x in (file.readline()).split()][0]
        file.close()
        file = open(f"tmp/{user_id}shadows.txt", "w")
        # Save colour in RGB
        color = self.DefaultShadows[f"{colortype}"][choice - 1]
        file.write(f"{color[0]} {color[1]} {color[2]}")
        file.close()
        return

    # Step of changing colours
    STEP = 1 / 8

    # Save custom user choice 1, 2, 3 или 4
    #    3
    # 1     2
    #    4
    # Call  this function only after SetDefaultshadows
    def SetCustomShadows(self,
                      user_id,
                      choice: int):
        # Get current colour
        file = open(f"tmp/{user_id}shadows.txt", "r")
        # Get previosly picked colour
        prev_color = [float(x) for x in (file.readline()).split()]
        # Go to the next colour in RGB
        color = (prev_color[0] / 255, prev_color[1] / 255, prev_color[2] / 255)
        color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        if choice == 1:
            color[2] = min(1, color[2] + self.STEP)
        elif choice == 2:
            color[2] = max(0, color[2] - self.STEP)
        elif choice == 3:
            color[1] = max(0, color[1] - self.STEP)
        elif choice == 4:
            color[1] = min(1, color[1] + self.STEP)
        color = colorsys.hsv_to_rgb(color[0], color[1], color[2])
        # Save new colour as current
        file = open(f"tmp/{user_id}shadows.txt", "w")
        file.write(f"{color[0] * 255} {color[1] * 255} {color[2] * 255}")
        file.close()
        return

    # Function that draw shadows
    def DrawShadows(self,
                 user_id):
        """ No error protection """
        # Читаем какого цвета губы рисовать
        # color = (255, 3, 53)
        try:
            file = open(f"tmp/{user_id}shadows.txt", "r")
            color = [float(x) for x in file.readline().split()]
            file.close()
        except:
            color = [-1, -1, -1]
        # Ищем лицо и точки
        img = cv2.imread(f"tmp/{user_id}res.jpg")
        # Если тени не выбраны - рисовать их не надо
        status = list(Status.Getstatus(user_id=user_id))
        if status[1] == 0:
            cv2.imwrite(f"tmp/{user_id}res.jpg", img)
            return
        # img = cv2.resize(img, (0, 0), None, 0.5, 0.5)
        # Черно белое для предиктора и детектора
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        faces = self.detector(gray_img)
        face = faces[0]
        landmarks = self.predictor(gray_img, face)
        points = []
        for n in range(68):
            x = landmarks.part(n).x
            y = landmarks.part(n).y
            points.append([x, y])
        points = np.array(points)
        # Делаем маску на тени(черно - белую) и обрезаем реальные губы (фон черный)
        cur_points1 = [
            points[36],
            mid(points[17], points[36], 1 / 3),
            mid(points[18], points[37], 1),
            mid(points[19], points[37], 1),
            mid(points[20], points[38], 1.5),
            mid(points[20], points[39], 4),
            points[39],
            points[38],
            points[37]
        ]
        cur_points2 = [
            points[45],
            mid(points[26], points[45], 1 / 3),
            mid(points[25], points[44], 1),
            mid(points[24], points[44], 1),
            mid(points[23], points[43], 1.5),
            mid(points[23], points[42], 4),
            points[42],
            points[43],
            points[44]
        ]
        cur_points1 = np.array(cur_points1)
        cur_points2 = np.array(cur_points2)
        mask1 = np.zeros_like(img)
        mask1 = cv2.fillPoly(mask1, [cur_points1], (255, 255, 255))
        mask2 = np.zeros_like(img)
        mask2 = cv2.fillPoly(mask2, [cur_points2], (255, 255, 255))
        mask = cv2.bitwise_or(mask1, mask2)
        real_shadows = cv2.bitwise_and(img, mask)
        shadows_mask = mask
        # colored_shadows - губы цвета color на черном фоне
        colored_shadows = np.zeros_like(shadows_mask)
        colored_shadows = cv2.bitwise_and(shadows_mask, colored_shadows)
        colored_shadows[:] = color
        colored_shadows = cv2.bitwise_and(colored_shadows, shadows_mask)
        # Результирующая картинка - result
        # Тут мы вычитаем реальные губы с картинки, перед этим размыв их
        result = cv2.addWeighted(img, 1, cv2.GaussianBlur(real_shadows, (21, 21), 1000), -1, 0)
        # Переводим цвет реальных губ в HSV
        real_shadows = cv2.cvtColor(real_shadows, cv2.COLOR_RGB2HSV)
        colored_shadows = cv2.cvtColor(colored_shadows, cv2.COLOR_RGB2HSV)
        # Говорим, что теперь реальные губы - это губы цвета color
        real_shadows[:, :, 0:3] = colored_shadows[:, :, 0:3]
        # Переводим новые реальные губы в RGB и размываем их
        real_shadows = cv2.cvtColor(real_shadows, cv2.COLOR_HSV2RGB)
        real_shadows = cv2.GaussianBlur(real_shadows, (21, 21), 1000)  ##was (5, 5), 10
        # opencv дурачок, считает что фото в BGR, а мы в BGR меняем только в крайнем случае
        # Когда рисуем, в частности
        real_shadows = real_shadows[:, :, ::-1]
        # Присобачиваем новые цветные губы на результат
        result = cv2.addWeighted(result, 1, real_shadows, 1, 0)
        # Записываем результат в файлик
        cv2.imwrite(f"tmp/{user_id}res.jpg", result)
        return

    # Для отрисовки оттенков на подложке
    dots = [
        ((205, 5), (395, 195)),
        ((205, 405), (395, 595)),
        ((5, 205), (195, 395)),
        ((405, 205), (595, 395)),
        ((205, 205), (395, 395))
    ]

    # Generate a oic with 2 new colours and the current one
    def GenerateNextRange(self,
                          user_id):
        # White background
        img = np.zeros((600, 600, 3), np.uint8)
        img[:] = (255, 255, 255)
        # Read chosen colour
        file = open(f"tmp/{user_id}shadows.txt")
        color = [float(x) for x in (file.readline()).split()]
        file.close()
        colors = []
        # Top in BGR
        color = (color[0] / 255, color[1] / 255, color[2] / 255)
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[2] = min(1, cur_color[2] + self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Bottom in BGR
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[2] = max(0, cur_color[2] - self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Left in BGR
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[1] = max(0, cur_color[1] - self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Right in BGR
        cur_color = [elem for elem in colorsys.rgb_to_hsv(color[0], color[1], color[2])]
        cur_color[1] = min(1, cur_color[1] + self.STEP)
        cur_color = colorsys.hsv_to_rgb(cur_color[0], cur_color[1], cur_color[2])
        cur_color = (cur_color[2], cur_color[1], cur_color[0])
        colors.append(cur_color)
        # Current colour in BGR
        colors.append((color[2], color[1], color[0]))
        # Draw colours
        for i in range(5):
            cv2.rectangle(
                img,
                self.dots[i][0], self.dots[i][1],
                (colors[i][0] * 255, colors[i][1] * 255, colors[i][2] * 255),
                cv2.FILLED)
        cv2.imwrite(f'tmp/{user_id}shadows.jpg', img)
        return



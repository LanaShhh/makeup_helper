# Makeup_helper

Данный репозиторий содержит все дополнительные материалы, которые были использованы в проекте "Бот-помощник для подбора макияжа"


## Описание файлов

[ColourDeterminator.py](https://gitlab.com/LanaShhh/makeup_helper/-/blob/main/ColourDeterminator.py) содержит код класса ColourDetermine, предназначенный для определения цветовых характеристик лица. Метод get_colours возвращает список из цвета глаз, лица, волос и бровей в RGB формате.

[VK_Bot.py](https://gitlab.com/LanaShhh/makeup_helper/-/blob/main/VK_Bot.py) содержит код бота ВКонтакте.

[GettingDataFrame.py](https://gitlab.com/LanaShhh/makeup_helper/-/blob/main/GettingDataFrame.py) - скрипт по считыванию параметров с фотографий и подготовке данных для обучения модели.

[MakeUpLipsMaker.py](https://gitlab.com/LanaShhh/makeup_helper/-/blob/main/MakeUpLipsMaker.py)  содержит класс, который отрисовывает помаду на губах, [MakeUpShadowsMaker.py](https://gitlab.com/LanaShhh/makeup_helper/-/blob/main/MakeUpShadowsMaker.py) - тени.

[Status.py](https://gitlab.com/LanaShhh/makeup_helper/-/blob/main/Status.py) - класс для запоминания необходимости нанести тени/помаду.

## Запуск и тестирование кода

Необходимые для запуска библиотеки описаны в файле [requirements.txt](https://gitlab.com/LanaShhh/makeup_helper/-/blob/main/requirements.txt).

Основной запускаемый файл - VK_Bot.py. Для запуска необходим токен сообщества ВКонтакте. Подробнее в [документации](https://dev.vk.com/api/access-token/getting-started) VK API. Для запуска на сервере рекомендуется хранить его как переменную окружения.

Токен необходимо присвоить переменной VK_TOKEN. После этого код можно запускать. Для взаимодействия с ботом нужно перейти на страницу сообщества ВКонтакте и отправить сообщение
